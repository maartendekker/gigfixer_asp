﻿using ClassLibary;
using GigFixer.DataLayer;
using GigFixer.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace TestProject
{
    [TestClass]
    public class IntergrationTestAccount
    {
        readonly Artist artist = new Artist(1, "artist@artist.com", null, null, null, null, 1, null, null);
        readonly Venue venue = new Venue(2, "venue@venue.com", null, null, null, null, 2, null, null, null, null, null);
        readonly string password = "Password1234";

        [TestMethod]
        public void IsArtistPositive()
        {
            AccountRepository accountRepository = new AccountRepository(new AccountTestContext());

            accountRepository.RegisterArtist(artist, password);

            Assert.IsTrue(accountRepository.IsArtist(artist.Id));
        }

        [TestMethod]
        public void IsArtistNegative()
        {
            AccountRepository accountRepository = new AccountRepository(new AccountTestContext());

            accountRepository.RegisterVenue(venue, password);

            Assert.IsFalse(accountRepository.IsArtist(venue.Id));
        }

        [TestMethod]
        public void RegisterArtistAndCheckValidLogin()
        {
            AccountRepository accountRepository = new AccountRepository(new AccountTestContext());

            accountRepository.RegisterArtist(artist, password);

            Assert.AreEqual(1, accountRepository.CheckValidLogin(artist.Email, password));
        }

        [TestMethod]
        public void RegisterVenueAndCheckValidLogin()
        {
            AccountRepository accountRepository = new AccountRepository(new AccountTestContext());

            accountRepository.RegisterVenue(venue, password);

            Assert.AreEqual(1, accountRepository.CheckValidLogin(venue.Email, password));
        }
    }
}
