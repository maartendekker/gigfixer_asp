﻿using ClassLibary;
using GigFixer.DataLayer;
using GigFixer.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace TestProject
{
    [TestClass]
    public class IntegrationTestVenue
    {
        [TestMethod]
        public void AddAndGetHall()
        {
            //arrange
            VenueRepository venueRepository = new VenueRepository(new VenueTestContext());

            Hall hall = new Hall(6, 9, "atrium", 300);

            //act
            venueRepository.AddHall(hall);

            //assert
            Assert.AreEqual(hall.Id, venueRepository.GetHallByHallId(6).Id);
        }

        [TestMethod]
        public void AddDeleteHall()
        {
            //arrange
            VenueRepository venueRepository = new VenueRepository(new VenueTestContext());
            Hall hall = new Hall(6, 9, "atrium", 300);

            //act
            venueRepository.AddHall(hall);
            venueRepository.DeleteHall(6);
            Hall hallactual = venueRepository.GetHallByHallId(6);

            //assert
            Assert.AreEqual(null, hallactual);
        }

        [TestMethod]
        public void GetVenueByAccountId()
        {
            //arrange
            VenueRepository venueRepository = new VenueRepository(new VenueTestContext());
            List<Hall> halls = new List<Hall>();
            Venue venueexpected = new Venue(20,
            "email@email.com",
            "Jake",
            "Smith",
            "",
            "+31620222086",
            9,
            "NiceStage",
            "City",
            "1234AB",
            "Summerway 41",
            halls);

            //act
            Venue venueactual = venueRepository.GetVenueByAccountId(20);

            //assert
            Assert.AreEqual(venueexpected.Id, venueactual.Id);
        }
    }
}
