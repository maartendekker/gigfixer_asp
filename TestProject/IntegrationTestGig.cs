using ClassLibary;
using GigFixer.DataLayer;
using GigFixer.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace TestProject
{
    [TestClass]
    public class IntegrationTestGig
    {
        [TestMethod]
        public void AddAndGetGig()
        {
            GigRepository gigRepository = new GigRepository(new GigTestContext());

            Gig gig = new Gig(0,
                              null,
                              null,
                              "JelleFest",
                              "Metal",
                              "Het laatste weekend van de vrijbuiter",
                              50,
                              DateTime.Now);

            List<Gig> gigsexpected = new List<Gig>
            {
                gig
            };
            gigRepository.AddGig(gig);

            List<Gig> gigsactual = new List<Gig>();
            gigsactual = gigRepository.GetGig();

            CollectionAssert.AreEqual((List<Gig>)gigsexpected, (List<Gig>)gigsactual);
        }

        [TestMethod]
        public void GetGigByGigId()
        {
            GigRepository gigRepository = new GigRepository(new GigTestContext());

            //arrange
            Gig gigexpected = new Gig(5,
                              null,
                              null,
                              "JelleFest",
                              "Metal",
                              "Het laatste weekend van de vrijbuiter",
                              50,
                              DateTime.Today);
            //act
            gigRepository.AddGig(gigexpected);

            //assert
            Assert.AreEqual(gigexpected, gigRepository.GetGigByGigId(5));
        }

        [TestMethod]
        public void DeleteGig()
        {
            //arrange
            GigRepository gigRepository = new GigRepository(new GigTestContext());

            Gig gig = new Gig(5,
                              null,
                              null,
                              "JelleFest",
                              "Metal",
                              "Het laatste weekend van de vrijbuiter",
                              50,
                              DateTime.Today);

            List<Gig> gigs = new List<Gig>();

            //act
            gigRepository.AddGig(gig);

            gigRepository.DeleteGig(5);

            List<Gig> gigsactual = new List<Gig>();
            gigsactual = gigRepository.GetGig();

            //assert
            CollectionAssert.AreEqual(gigs, gigsactual);
        }
    }
}
