﻿using ClassLibary;
using GigFixer.DataLayer.Register;
using GigFixer.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace TestProject
{
    [TestClass]
    public class IntegrationTestRegistration
    {
        [TestMethod]
        public void GetRegistrationsForArtist()
        {
            //arrange
            RegistrationRepository registrationRepository = new RegistrationRepository(new RegistrationTestContext());
            List<Registration> regsactual = new List<Registration>();

            //act
            regsactual = registrationRepository.GetRegistrationForArtist(5);

            //assert
            Assert.AreEqual(2, regsactual.Count);
        }

        [TestMethod]
        public void GetRegistrationsForVenue()
        {
            //arrange
            RegistrationRepository registrationRepository = new RegistrationRepository(new RegistrationTestContext());
            List<Registration> regsactual = new List<Registration>();

            //act
            regsactual = registrationRepository.GetRegistrationForVenue(7);

            //assert
            Assert.AreEqual(2, regsactual.Count);
        }
    }
}
