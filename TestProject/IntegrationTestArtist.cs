﻿using ClassLibary;
using GigFixer.DataLayer;
using GigFixer.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace TestProject
{
    [TestClass]
    public class IntegrationTestArtist
    {
        [TestMethod]
        public void GetArtistByAccountId()
        {
            //arrange
            ArtistRepository artistRepository = new ArtistRepository(new ArtistTestContext());
            Artist artist = new Artist(
                8,
                "email@email.com",
                "Sjon",
                "Schaap",
                "het",
                "06589425",
                4,
                "TheAmazingSheep",
                "SheepCore");

            //act
            Artist returnartist = artistRepository.GetArtistByAccountId(artist.Id);

            //assert
            Assert.AreEqual(artist.Id, returnartist.Id);

        }

    }
}
