﻿using ClassLibary;
using GigFixer.DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GigFixer.Repositories
{
    public class VenueRepository
    {
        readonly IVenueContext _context;

        public VenueRepository(IVenueContext context)
        {
            _context = context;
        }

        public Venue GetVenueByAccountId(int accountId)
        {
            Venue venue = _context.GetVenueByAccountId(accountId);
            return venue;
        }

        public void DeleteHall(int hallId)
        {
            _context.DeleteHall(hallId);
        }

        public void AddHall(Hall hall)
        {
            _context.AddHall(hall);
        }

        public Hall GetHallByHallId(int hallId)
        {
            Hall hall = _context.GetHallByHallId(hallId);
            return hall;
        }
    }
}
