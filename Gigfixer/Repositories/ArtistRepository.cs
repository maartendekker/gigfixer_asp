﻿using ClassLibary;
using GigFixer.DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GigFixer.Repositories
{
    public class ArtistRepository
    {
        readonly IArtistContext _context;

        public ArtistRepository(IArtistContext context)
        {
            _context = context;
        }

        public Artist GetArtistByAccountId(int accountId)
        {
            Artist artist = _context.GetArtistByAccountId(accountId);
            return artist;
        }

    }
}
