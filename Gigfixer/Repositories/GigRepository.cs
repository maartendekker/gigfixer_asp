﻿using ClassLibary;
using GigFixer.DataLayer.Event;
using GigFixer.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GigFixer.Repositories
{
    public class GigRepository
    {
        readonly IGigContext _context;

        public GigRepository(IGigContext context)
        {
            _context = context;
        }

        public void AddGig(Gig gig)
        {
            _context.AddGig(gig);
        }

        public List<Gig> GetGig()
        {
            List<Gig> gigs = _context.GetGig();
            return gigs;
        }

        public Gig GetGigByGigId(int gigid)
        {
            Gig gig = _context.GetGigByGigId(gigid);
            return gig;
        }

        public int GetRegisterdCount(int artistId)
        {
            int count = _context.GetRegisterdCount(artistId);
            return count;
        }

        public List<Gig> GetRegisterdGigs(int artistId)
        {
            List<Gig> gigs = _context.GetRegisterdGigs(artistId);
            return gigs;
        }

        public List<Gig> GetGigForVenue(int venueId)
        {
            List<Gig> gigs = _context.GetGigForVenue(venueId);
            return gigs;
        }

        public void DeleteGig(int gigid)
        {
            _context.DeleteGig(gigid);
        }
    }
}
