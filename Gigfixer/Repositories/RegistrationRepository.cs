﻿using ClassLibary;
using GigFixer.DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GigFixer.Repositories
{
    public class RegistrationRepository
    {
        readonly IRegistrationContext _context;

        public RegistrationRepository(IRegistrationContext context)
        {
            _context = context;
        }

        public void Register(Registration registration)
        {
            _context.Register(registration);
        }

        public List<Registration> GetRegistrationForArtist(int artistId)
        {
            List<Registration> list = new List<Registration>();
            list = _context.GetRegistrationForArtist(artistId);
            return list;
        }

        public List<Registration> GetRegistrationForVenue(int venueId)
        {
            List<Registration> list = new List<Registration>();
            list = _context.GetRegistrationForVenue(venueId);
            return list;
        }

        public void DeleteRegistration(int regId)
        {
            _context.DeleteRegistration(regId);
            return;
        }

        public void ApproveRegistration(int regId)
        {
            _context.ApproveRegistration(regId);
            return;
        }

        internal void DeclineRegistration(int regId)
        {
            _context.DeclineRegistration(regId);
            return;
        }
    }
}
