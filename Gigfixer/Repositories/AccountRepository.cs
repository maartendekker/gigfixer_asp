﻿using ClassLibary;
using GigFixer.DataLayer;
using GigFixer.ViewModels;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace GigFixer.Repositories
{
    public class AccountRepository
    {
        readonly IAccountContext _context;

        public AccountRepository(IAccountContext context)
        {
            _context = context;
        }

        public int? CheckValidLogin(string email, string password)
        {
            return _context.CheckValidLogin(email, password);
        }

        public bool IsArtist(int? loginid)
        {
            return _context.IsArtist(loginid);
        }

        public void RegisterArtist(Artist artist, string password)
        {
            //Geneate a salt
            byte[] salt = new byte[128 / 8];
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(salt);
            }

            //Hash the password
            string hashedPassword = Convert.ToBase64String(KeyDerivation.Pbkdf2(
            password: password,
            salt: salt,
            prf: KeyDerivationPrf.HMACSHA256,
            iterationCount: 10000,
            numBytesRequested: 256 / 8));

            _context.RegisterArtist(artist, salt, hashedPassword);
        }

        public void RegisterVenue(Venue venue, string password)
        {
            //Geneate a salt
            byte[] salt = new byte[128 / 8];
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(salt);
            }

            //Hash the password
            string hashedPassword = Convert.ToBase64String(KeyDerivation.Pbkdf2(
            password: password,
            salt: salt,
            prf: KeyDerivationPrf.HMACSHA256,
            iterationCount: 10000,
            numBytesRequested: 256 / 8));

            _context.RegisterVenue(venue, salt, hashedPassword);
        }
    }
}
