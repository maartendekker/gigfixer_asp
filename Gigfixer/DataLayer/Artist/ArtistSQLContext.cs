﻿using ClassLibary;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace GigFixer.DataLayer
{
    public class ArtistSQLContext : BaseSQLContext, IArtistContext
    {
        public Artist GetArtistByAccountId(int userId)
        {
            string query = "SELECT " +
            "Gigfixer.[Artist].name as artistname, " +
            "Gigfixer.[Artist].genre as genre, " +
            "Gigfixer.[Artist].id as artistId, " +
            "Gigfixer.[User].id as id, " +
            "Gigfixer.[User].email as email, " +
            "Gigfixer.[User].firstName as firstName, " +
            "Gigfixer.[User].infix as infix, " +
            "Gigfixer.[User].lastName as lastName, " +
            "Gigfixer.[User].telNr as telNr " +
            "FROM " +
            "Gigfixer.[Artist] " +
            "INNER JOIN Gigfixer.[User] on Gigfixer.[Artist].userId = Gigfixer.[User].id " +
            "WHERE " +
            "Gigfixer.[Artist].userId = @userId";

            SqlCommand command = CreateSQLCommand(query, new List<KeyValuePair<string, object>>()
            {
                new KeyValuePair<string, object>("@userId", userId)
            });

            List<Artist> artists = ModelFiller.FillArtist(command);

            if (artists.Count > 0)
            {
                return artists[0];
            }
            else
            {
                return null;
            }
        }
    }
}
