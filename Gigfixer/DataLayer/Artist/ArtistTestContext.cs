﻿using System;
using ClassLibary;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GigFixer.DataLayer
{
    public class ArtistTestContext : IArtistContext
    {
        public Artist GetArtistByAccountId(int userId)
        {
            //arrange mock data
            List<Artist> artists = new List<Artist>();
            Artist artist = new Artist(
                8,
                "email@email.com",
                "Sjon",
                "Schaap",
                "het",
                "06589425",
                4,
                "TheAmazingSheep",
                "SheepCore");

            artists.Add(artist);

            //the function
            Artist returnartist;
            returnartist = artists.Find(f => f.Id == userId);

            return returnartist;
        }
    }
}
