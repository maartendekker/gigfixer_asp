﻿using ClassLibary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GigFixer.DataLayer
{
    public interface IArtistContext
    {
        Artist GetArtistByAccountId(int accountId);
    }
}
