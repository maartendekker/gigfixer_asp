﻿using ClassLibary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GigFixer.DataLayer
{
    public interface IVenueContext
    {
        Venue GetVenueByAccountId(int accountId);

        void DeleteHall(int hallId);

        void AddHall(Hall hall);

        Hall GetHallByHallId(int hallId);
    }
}