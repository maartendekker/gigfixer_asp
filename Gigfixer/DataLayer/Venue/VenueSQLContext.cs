﻿using ClassLibary;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace GigFixer.DataLayer
{
    public class VenueSQLContext : BaseSQLContext, IVenueContext
    {
        public void AddHall(Hall hall)
        {
            string query = "INSERT INTO Gigfixer.[Hall](venueId, name, capacity) " +
                           "VALUES (@venueId, @name, @capacity)";

            SqlCommand command = CreateSQLCommand(query, new List<KeyValuePair<string, object>>()
            {
                new KeyValuePair<string, object>("@venueId", hall.VenueId),
                new KeyValuePair<string, object>("@name", hall.Name),
                new KeyValuePair<string, object>("@capacity", hall.Capacity)
            });

            ExecuteNonQuery(command);
        }

        public void DeleteHall(int hallId)
        {
            string query = "DELETE FROM Gigfixer.[Hall] WHERE id = @hallId";

            SqlCommand command = CreateSQLCommand(query, new List<KeyValuePair<string, object>>()
            {
                new KeyValuePair<string, object>("@hallId", hallId)
            });

            ExecuteNonQuery(command);
        }

        public Hall GetHallByHallId(int hallId)
        {
            string query = "SELECT * FROM Gigfixer.[Hall] WHERE id = @hallId";

            SqlCommand command = CreateSQLCommand(query, new List<KeyValuePair<string, object>>()
            {
                new KeyValuePair<string, object>("@hallId", hallId)
            });

            int id = 0;
            int venueId = 0;
            string name = null;
            int capacity = 0;

            using (command)
            {
                try
                {
                    command.Connection.Open();

                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        if (reader.HasRows)
                        {
                            id = Convert.ToInt32(reader["id"]);
                            venueId = Convert.ToInt32(reader["venueId"]);
                            name = Convert.ToString(reader["name"]);
                            capacity = Convert.ToInt32(reader["capacity"]);
                        }
                    }

                    command.Connection.Close();
                }
                catch (Exception ex)
                {
                    Console.Write("Error: " + ex.Message);
                }
            }
            return new Hall(id, venueId, name, capacity);
        }

        public Venue GetVenueByAccountId(int accountId)
        {
            string query = "SELECT " +
            "Gigfixer.[Venue].id as venueid, " +
            "Gigfixer.[Venue].name as venuename, " +
            "Gigfixer.[Venue].city as city, " +
            "Gigfixer.[Venue].postalcode as postalcode, " +
            "Gigfixer.[Venue].address as address, " +
            "Gigfixer.[Venue].id as venueId, " +
            "Gigfixer.[User].id as id, " +
            "Gigfixer.[User].email as email, " +
            "Gigfixer.[User].firstName as firstName, " +
            "Gigfixer.[User].infix as infix, " +
            "Gigfixer.[User].lastName as lastName, " +
            "Gigfixer.[User].telNr as telNr, " +
            "Gigfixer.[Hall].id as hallId, " +
            "Gigfixer.[Hall].venueId as venueId, " +
            "Gigfixer.[Hall].name as name, " +
            "Gigfixer.[Hall].capacity as capacity " +
            "FROM " +
            "Gigfixer.[Venue] " +
            "INNER JOIN Gigfixer.[User] on Gigfixer.[Venue].userId = Gigfixer.[User].id " +
            "INNER JOIN Gigfixer.[Hall] on Gigfixer.[Venue].id = Gigfixer.[Hall].venueId " +
            "WHERE " +
            "Gigfixer.[Venue].userId = @userId";

            SqlCommand command = CreateSQLCommand(query, new List<KeyValuePair<string, object>>()
            {
                new KeyValuePair<string, object>("@userId", accountId)
            });

            Venue venue;
            venue = null;
            List<Hall> halls = new List<Hall>();

            int userId = 0;
            string email = "";
            string firstName = "";
            string lastName = "";
            string infix = "";
            string telNr = "";
            int venueId = 0;
            string venueName = "";
            string city = "";
            string postalcode = "";
            string address = "";

            try
            {
                command.Connection.Open();

                SqlDataReader reader = command.ExecuteReader();

                //store the data
                while (reader.Read())
                {
                    //halls

                    halls.Add(new Hall(
                        Convert.ToInt32(reader["hallId"]),
                        Convert.ToInt32(reader["venueId"]),
                        Convert.ToString(reader["name"]),
                        Convert.ToInt32(reader["capacity"])));

                    //User

                    userId = Convert.ToInt32(reader["id"]);
                    email = Convert.ToString(reader["email"]);
                    firstName = Convert.ToString(reader["firstName"]);
                    lastName = Convert.ToString(reader["lastName"]);
                    infix = Convert.ToString(reader["infix"]);
                    telNr = Convert.ToString(reader["telNr"]);

                    //Venue

                    venueId = Convert.ToInt32(reader["venueId"]);
                    venueName = Convert.ToString(reader["venuename"]);
                    city = Convert.ToString(reader["city"]);
                    postalcode = Convert.ToString(reader["postalcode"]);
                    address = Convert.ToString(reader["address"]);
                }
                command.Connection.Close();
            }
            catch (Exception ex)
            {
                Console.Write("Error: " + ex.Message);
            }


            //create venue object
            venue = new Venue(
                        userId,
                        email,
                        firstName,
                        lastName,
                        infix,
                        telNr,
                        venueId,
                        venueName,
                        city,
                        postalcode,
                        address,
                        halls);

            if (venue != null)
            {
                return venue;
            }

            return null;
        }


    }
}
