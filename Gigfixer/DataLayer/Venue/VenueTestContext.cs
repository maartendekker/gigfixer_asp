﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ClassLibary;

namespace GigFixer.DataLayer
{
    public class VenueTestContext : IVenueContext
    {
        public static List<Hall> halls = new List<Hall>();
        public List<Venue> venues = new List<Venue>();
        readonly Venue venue = new Venue(
            20,
            "email@email.com",
            "Jake",
            "Smith",
            "",
            "+31620222086",
            9,
            "NiceStage",
            "City",
            "1234AB",
            "Summerway 41",
            halls);

        public void AddHall(Hall hall)
        {
            halls.Add(hall);
        }

        public void DeleteHall(int hallId)
        {
            halls.Clear();
            Hall toremove = halls.Find(f => f.Id == hallId);
            halls.Remove(toremove);
        }

        public Hall GetHallByHallId(int hallId)
        {
            return halls.Find(f => f.Id == hallId);
        }

        public Venue GetVenueByAccountId(int accountId)
        {
            venues.Clear();
            venues.Add(venue);
            return venues.Find(f => f.Id == accountId);
        }
    }
}
