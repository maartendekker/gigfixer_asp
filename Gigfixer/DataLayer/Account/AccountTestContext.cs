﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ClassLibary;
using GigFixer.ViewModels;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;

namespace GigFixer.DataLayer
{
    public class AccountTestContext : IAccountContext
    {
        public List<Artistsalthashed> artistsalthasheds = new List<Artistsalthashed>();
        public List<Venuesalthashed> venuesalthasheds = new List<Venuesalthashed>();
        public int? CheckValidLogin(string email, string password)
        {
            if (artistsalthasheds.Exists(f => f.artist.Email == email))
            {

                Artist artist = artistsalthasheds.Find(f => f.artist.Email == email).artist;
                int? integer;

                string hashedPassword = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                password: password,
                salt: artistsalthasheds.Find(f => f.artist == artist).salt,
                prf: KeyDerivationPrf.HMACSHA256,
                iterationCount: 10000,
                numBytesRequested: 256 / 8
                ));

                if (hashedPassword == artistsalthasheds.Find(f => f.artist == artist).hashedpassword)
                {
                    integer = 1;
                }
                else
                {
                    integer = null;
                }

                artistsalthasheds.Clear();
                return integer;
            }
            else
            {
                Venue venue = venuesalthasheds.Find(f => f.venue.Email == email).venue;
                int? integer;

                string hashedPassword = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                password: password,
                salt: venuesalthasheds.Find(f => f.venue == venue).salt,
                prf: KeyDerivationPrf.HMACSHA256,
                iterationCount: 10000,
                numBytesRequested: 256 / 8
                ));

                if (hashedPassword == venuesalthasheds.Find(f => f.venue == venue).hashedpassword)
                {
                    integer = 1;
                }
                else
                {
                    integer = null;
                }

                venuesalthasheds.Clear();
                return integer;
            }
        }

        public bool IsArtist(int? loginid)
        {
            if (artistsalthasheds.Exists(f => f.artist.Id == loginid))
            {
                artistsalthasheds.Clear();
                return true;
            }
            else
            {
                artistsalthasheds.Clear();
                return false;
            }
        }

        public void RegisterArtist(Artist artist, byte[] salt, string hashedPassword)
        {
            artistsalthasheds.Add(new Artistsalthashed(artist, salt, hashedPassword));
        }

        public void RegisterVenue(Venue venue, byte[] salt, string hashedPassword)
        {
            venuesalthasheds.Add(new Venuesalthashed(venue, salt, hashedPassword));
        }
    }

    public class Artistsalthashed
    {
        public Artistsalthashed(Artist artist, byte[] salt, string hashedpassword)
        {
            this.artist = artist;
            this.salt = salt;
            this.hashedpassword = hashedpassword;
        }
        public Artist artist;
        public byte[] salt;
        public string hashedpassword;
    }

    public class Venuesalthashed
    {
        public Venuesalthashed(Venue venue, byte[] salt, string hashedpassword)
        {
            this.venue = venue;
            this.salt = salt;
            this.hashedpassword = hashedpassword;
        }
        public Venue venue;
        public byte[] salt;
        public string hashedpassword;
    }
}
