﻿using GigFixer.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using GigFixer.ViewModels;
using ClassLibary;

namespace GigFixer.DataLayer
{
    class AccountSQLContext : BaseSQLContext, IAccountContext
    {
        public int? CheckValidLogin(string email, string password)
        {
            //get salt
            string query = "SELECT salt as salt FROM Gigfixer.[User] WHERE email = @email";
            SqlCommand command = CreateSQLCommand(query);

            command.Parameters.Add("@email", SqlDbType.VarChar);
            command.Parameters["@email"].Value = email;

            byte[] salt = null;

            try
            {
                command.Connection.Open();
                SqlDataReader reader = command.ExecuteReader();

                if (reader.Read())
                {
                    salt = (byte[])reader["salt"];
                }
                else
                {
                    command.Connection.Close();
                    return null;
                }

                command.Connection.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
            

            //Hash the entered password with the retrieved salt
            string hashedPassword = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                password: password,
                salt: salt,
                prf: KeyDerivationPrf.HMACSHA256,
                iterationCount: 10000,
                numBytesRequested: 256 / 8
            ));

            command.CommandText = "SELECT * FROM Gigfixer.[User] WHERE email = @_email AND password LIKE @password";
            
            command.Parameters.Add("@password", SqlDbType.Text);
            command.Parameters["@password"].Value = hashedPassword;
            command.Parameters.AddWithValue("@_email", email);

            int? returnint = null;

            try
            {
                command.Connection.Open();
                SqlDataReader _reader = command.ExecuteReader();
                if (_reader.Read())
                {
                    int userid = Convert.ToInt32(_reader["id"]);

                    command.Connection.Close();
                    returnint = userid;
                }
                else
                {
                    command.Connection.Close();
                    returnint = null;
                }
            }
            catch (Exception ex)
            {
                Console.Write("Error: " + ex.Message);
            }
            return returnint;

        }

        public bool IsArtist(int? loginid)
        {
            string query = "SELECT id FROM Gigfixer.[Artist] WHERE userId = @userid";
            SqlCommand command = CreateSQLCommand(query, new List<KeyValuePair<string, object>>()
            {
                new KeyValuePair<string, object>("@userid", loginid)
            });

            bool returntype = ModelFiller.Returntype(command);

            return returntype;
        }

        public void RegisterArtist(Artist artist, byte[] salt, string hashedPassword)
        {
            string query = "INSERT INTO Gigfixer.[User](email, password, salt, firstName, lastName, infix, telNr) " +
                           "OUTPUT INSERTED.ID " +
                           "VALUES (@emailaddress, @password, @salt, @firstname, @lastname, @infix, @telNr)";

            SqlCommand commandU = CreateSQLCommand(query, new List<KeyValuePair<string, object>>()
            {
                new KeyValuePair<string, object>("@emailaddress", artist.Email),
                new KeyValuePair<string, object>("@password", hashedPassword),
                new KeyValuePair<string, object>("@salt", salt),
                new KeyValuePair<string, object>("@firstname", artist.FirstName),
                new KeyValuePair<string, object>("@infix", artist.Infix),
                new KeyValuePair<string, object>("@lastname", artist.LastName),
                new KeyValuePair<string, object>("@telNr", artist.TelNr)
            });

            Int32 userId = 0;

            foreach (SqlParameter Parameter in commandU.Parameters)
            {
                if (Parameter.Value == null)
                {
                    Parameter.Value = DBNull.Value;
                }
            }

            try
            {
                commandU.Connection.Open();
                userId = (Int32)commandU.ExecuteScalar();
                commandU.Connection.Close();
            }
            catch (Exception ex)
            {
                Console.Write("Error: " + ex.Message);
            }

            string queryartist = "INSERT INTO Gigfixer.[Artist](userId, name, genre)" +
                                 "VALUES (@userId, @name, @genre)";

            SqlCommand commandA = CreateSQLCommand(queryartist, new List<KeyValuePair<string, object>>()
            {
                new KeyValuePair<string, object>("@userId", userId),
                new KeyValuePair<string, object>("@name", artist.Name),
                new KeyValuePair<string, object>("@genre", artist.Genre)
            });

            ExecuteNonQuery(commandA);
        }

        public void RegisterVenue(Venue venue, byte[] salt, string hashedPassword)
        {
            string queryuser = "INSERT INTO Gigfixer.[User](email, password, salt, firstName, lastName, infix, telNr) " +
                               "OUTPUT INSERTED.ID " +
                               "VALUES (@emailaddress, @password, @salt, @firstname, @lastname, @infix, @telNr)";

            SqlCommand commandU = CreateSQLCommand(queryuser, new List<KeyValuePair<string, object>>()
            {
                new KeyValuePair<string, object>("@emailaddress", venue.Email),
                new KeyValuePair<string, object>("@password", hashedPassword),
                new KeyValuePair<string, object>("@salt", salt),
                new KeyValuePair<string, object>("@firstname", venue.FirstName),
                new KeyValuePair<string, object>("@lastname", venue.LastName),
                new KeyValuePair<string, object>("@infix", venue.Infix),
                new KeyValuePair<string, object>("@telNr", venue.TelNr)
            });

            Int32 userId = 0;

            foreach (SqlParameter Parameter in commandU.Parameters)
            {
                if (Parameter.Value == null)
                {
                    Parameter.Value = DBNull.Value;
                }
            }

            try
            {
                commandU.Connection.Open();
                userId = (Int32)commandU.ExecuteScalar();
                commandU.Connection.Close();
            }
            catch (Exception ex)
            {
                Console.Write("Error: " + ex.Message);
            }

            string queryvenue = "INSERT INTO Gigfixer.[Venue](userId, name, city, postalcode, address)" +
                                "OUTPUT INSERTED.ID " +
                                "VALUES (@userId, @name, @city, @postal, @address)";

            SqlCommand commandV = CreateSQLCommand(queryvenue, new List<KeyValuePair<string, object>>()
            {
                new KeyValuePair<string, object>("@userId", userId),
                new KeyValuePair<string, object>("@name", venue.Name),
                new KeyValuePair<string, object>("@city", venue.City),
                new KeyValuePair<string, object>("@postal", venue.PostalCode),
                new KeyValuePair<string, object>("@address", venue.Address)
            });

            Int32 venueId = 0;

            try
            {
                commandV.Connection.Open();
                venueId = (Int32)commandV.ExecuteScalar();
                commandV.Connection.Close();
            }
            catch (Exception ex)
            {
                Console.Write("Error: " + ex.Message);
            }


            string queryhall = "INSERT INTO Gigfixer.[Hall](venueId, name, capacity)" +
                               "VALUES (@venueId, @name, @capacity)";

            SqlCommand commandH = CreateSQLCommand(queryhall, new List<KeyValuePair<string, object>>()
            {
                new KeyValuePair<string, object>("@venueId", venueId),
                new KeyValuePair<string, object>("@name", venue.Halls.First().Name),
                new KeyValuePair<string, object>("@capacity", venue.Halls.First().Capacity)
            });

            ExecuteNonQuery(commandH);
        }
    }
}
