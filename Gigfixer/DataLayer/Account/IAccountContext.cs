﻿
using ClassLibary;
using GigFixer.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GigFixer.DataLayer
{
    public interface IAccountContext
    {
        int? CheckValidLogin(string email, string password);
       
        void RegisterArtist(Artist artist, byte[] salt, string hashedPassword);

        void RegisterVenue(Venue venue, byte[] salt, string hashedPassword);

        bool IsArtist(int? loginid);
    }
}
