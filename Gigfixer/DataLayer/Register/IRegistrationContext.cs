﻿using GigFixer.ViewModels;
using ClassLibary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GigFixer.DataLayer
{
    public interface IRegistrationContext
    {
        void Register(Registration registration);
        List<Registration> GetRegistrationForArtist(int artistId);
        List<Registration> GetRegistrationForVenue(int venueId);
        void DeleteRegistration(int regId);
        void ApproveRegistration(int regId);
        void DeclineRegistration(int regId);
    }
}
