﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ClassLibary;

namespace GigFixer.DataLayer.Register
{
    public class RegistrationTestContext : IRegistrationContext
    {
        public Registration registration1 = new Registration(10, new Artist(1, null, null, null, null, null, 5, null, null), new Gig(1, new Venue(0, null, null, null, null, null, 5, null, null, null, null, null), null, null, null, null, 0, DateTime.Now), "jep", false);
        public Registration registration2 = new Registration(10, new Artist(2, null, null, null, null, null, 5, null, null), new Gig(1, new Venue(0, null, null, null, null, null, 7, null, null, null, null, null), null, null, null, null, 0, DateTime.Now), "jep", false);
        public Registration registration3 = new Registration(10, new Artist(3, null, null, null, null, null, 7, null, null), new Gig(1, new Venue(0, null, null, null, null, null, 7, null, null, null, null, null), null, null, null, null, 0, DateTime.Now), "jep", false);

        public List<Registration> registrations = new List<Registration>();

        public void ApproveRegistration(int regId)
        {
            //registration = new Registration(10, null, null, "jep", false);
            //registration.Approved = true;

            throw new NotImplementedException();
        }

        public void DeclineRegistration(int regId)
        {
            throw new NotImplementedException();
        }

        public void DeleteRegistration(int regId)
        {
            throw new NotImplementedException();
        }

        public List<Registration> GetRegistrationForArtist(int artistId)//5
        {
            registrations.Add(registration1);
            registrations.Add(registration2);
            registrations.Add(registration3);

            List<Registration> regsforartist = new List<Registration>();

            regsforartist = registrations.FindAll(f => f.Artist.ArtistId == artistId);

            return regsforartist;

        }

        public List<Registration> GetRegistrationForVenue(int venueId)
        {
            registrations.Add(registration1);
            registrations.Add(registration2);
            registrations.Add(registration3);

            List<Registration> regsforvenue = new List<Registration>();

            regsforvenue = registrations.FindAll(f => f.Gig.Venue.VenueId == venueId);

            return regsforvenue;
        }

        public void Register(Registration registration)
        {
            throw new NotImplementedException();
        }
    }
}
