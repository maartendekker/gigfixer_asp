﻿using ClassLibary;
using GigFixer.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace GigFixer.DataLayer
{
    public class RegistrationSQLContext : BaseSQLContext, IRegistrationContext
    {
        public void Register(Registration registration)
        {
            string query = "INSERT INTO Gigfixer.[Registration](artistId, gigId, description, approved) " +
                           "VALUES (@artistId, @gigId, @description, @approved)";

            SqlCommand command = CreateSQLCommand(query, new List<KeyValuePair<string, object>>()
            {
                new KeyValuePair<string, object>("@artistId", registration.Artist.ArtistId),
                new KeyValuePair<string, object>("@gigId", registration.Gig.Id),
                new KeyValuePair<string, object>("@description", registration.Description),
                new KeyValuePair<string, object>("@approved", registration.Approved)
            });

            ExecuteNonQuery(command);
        }
        public List<Registration> GetRegistrationForArtist(int artistId)
        {
            string query = "SELECT " +
            "Gigfixer.[Registration].id as regId, " +
            "Gigfixer.[Registration].artistId as artistId, " +
            "Gigfixer.[Registration].gigId as regGigId, " +
            "Gigfixer.[Registration].description as regDesc, " +
            "Gigfixer.[Registration].approved as approved, " +
            "Gigfixer.[Artist].userId as userId, " +
            "Gigfixer.[Artist].name as artistName, " +
            "Gigfixer.[Artist].genre as artistGenre, " +
            "Gigfixer.[User].email as email, " +
            "Gigfixer.[User].firstName as firstName, " +
            "Gigfixer.[User].lastName as lastName, " +
            "Gigfixer.[User].infix as infix, " +
            "Gigfixer.[User].telNr as telNr, " +
            "Gigfixer.[Gig].Id as gigId, " +
            "Gigfixer.[Gig].venueId as venueId, " +
            "Gigfixer.[Gig].hallId as hallId, " +
            "Gigfixer.[Gig].name as gigName, " +
            "Gigfixer.[Gig].genre as gigGenre, " +
            "Gigfixer.[Gig].description as gigDesc, " +
            "Gigfixer.[Gig].wage as wage, " +
            "Gigfixer.[Gig].date as date, " +
            "Gigfixer.[Venue].name as venuename, " +
            "Gigfixer.[Venue].city as city, " +
            "Gigfixer.[Venue].address as address, " +
            "Gigfixer.[Venue].postalcode as postalcode, " +
            "Gigfixer.[Hall].id as hallId, " +
            "Gigfixer.[Hall].name as hallname, " +
            "Gigfixer.[Hall].capacity as capacity " +
            "FROM " +
            "Gigfixer.[Registration] " +
            "INNER JOIN Gigfixer.[Artist] on Gigfixer.[Artist].id = Gigfixer.[Registration].artistId " +
            "INNER JOIN Gigfixer.[User] on Gigfixer.[User].id = Gigfixer.[Artist].userId " +
            "INNER JOIN Gigfixer.[Gig] on Gigfixer.[Gig].id = Gigfixer.[Registration].gigId " +
            "INNER JOIN Gigfixer.[Venue] on Gigfixer.[Venue].id = Gigfixer.[Gig].venueId " +
            "INNER JOIN Gigfixer.[Hall] on Gigfixer.[Hall].id = Gigfixer.[Gig].hallId " +
            "WHERE Gigfixer.[Registration].artistId = @artistId";

            SqlCommand command = CreateSQLCommand(query, new List<KeyValuePair<string, object>>()
            {
                new KeyValuePair<string, object>("@artistId", artistId)
            });

            List<Registration> registrations = ModelFiller.FillRegistrations(command);

            return registrations;
        }

        public List<Registration> GetRegistrationForVenue(int venueid)
        {
            string query = "SELECT " +
            "Gigfixer.[Registration].id as regId, " +
            "Gigfixer.[Registration].artistId as artistId, " +
            "Gigfixer.[Registration].gigId as regGigId, " +
            "Gigfixer.[Registration].description as regDesc, " +
            "Gigfixer.[Registration].approved as approved, " +
            "Gigfixer.[Artist].userId as userId, " +
            "Gigfixer.[Artist].name as artistName, " +
            "Gigfixer.[Artist].genre as artistGenre, " +
            "Gigfixer.[User].email as email, " +
            "Gigfixer.[User].firstName as firstName, " +
            "Gigfixer.[User].lastName as lastName, " +
            "Gigfixer.[User].infix as infix, " +
            "Gigfixer.[User].telNr as telNr, " +
            "Gigfixer.[Gig].Id as gigId, " +
            "Gigfixer.[Gig].venueId as venueId, " +
            "Gigfixer.[Gig].hallId as hallId, " +
            "Gigfixer.[Gig].name as gigName, " +
            "Gigfixer.[Gig].genre as gigGenre, " +
            "Gigfixer.[Gig].description as gigDesc, " +
            "Gigfixer.[Gig].wage as wage, " +
            "Gigfixer.[Gig].date as date, " +
            "Gigfixer.[Venue].name as venuename, " +
            "Gigfixer.[Venue].city as city, " +
            "Gigfixer.[Venue].address as address, " +
            "Gigfixer.[Venue].postalcode as postalcode, " +
            "Gigfixer.[Hall].id as hallId, " +
            "Gigfixer.[Hall].name as hallname, " +
            "Gigfixer.[Hall].capacity as capacity " +
            "FROM " +
            "Gigfixer.[Registration] " +
            "INNER JOIN Gigfixer.[Artist] on Gigfixer.[Artist].id = Gigfixer.[Registration].artistId " +
            "INNER JOIN Gigfixer.[User] on Gigfixer.[User].id = Gigfixer.[Artist].userId " +
            "INNER JOIN Gigfixer.[Gig] on Gigfixer.[Gig].id = Gigfixer.[Registration].gigId " +
            "INNER JOIN Gigfixer.[Venue] on Gigfixer.[Venue].id = Gigfixer.[Gig].venueId " +
            "INNER JOIN Gigfixer.[Hall] on Gigfixer.[Hall].id = Gigfixer.[Gig].hallId " +
            "WHERE Gigfixer.[Gig].venueId = @venueid";

            SqlCommand command = CreateSQLCommand(query, new List<KeyValuePair<string, object>>()
            {
                new KeyValuePair<string, object>("@venueid", venueid)
            });

            List<Registration> list = new List<Registration>();
            list = ModelFiller.FillRegistrations(command);

            return list;
        }

        public void DeleteRegistration(int regId)
        {
            string query = "DELETE FROM Gigfixer.[Registration] WHERE id = @regId";

            SqlCommand command = CreateSQLCommand(query, new List<KeyValuePair<string, object>>()
            {
                new KeyValuePair<string, object>("@regId", regId)
            });

            ExecuteNonQuery(command);
        }

        public void ApproveRegistration(int regId)
        {
            string query = "UPDATE Gigfixer.[Registration] " +
                           "SET approved = 1" +
                           "WHERE id = @regId";

            SqlCommand command = CreateSQLCommand(query, new List<KeyValuePair<string, object>>()
            {
                new KeyValuePair<string, object>("@regId", regId)
            });

            ExecuteNonQuery(command);
        }

        public void DeclineRegistration(int regId)
        {
            string query = "UPDATE Gigfixer.[Registration] " +
                           "SET approved = 0" +
                           "WHERE id = @regId";

            SqlCommand command = CreateSQLCommand(query, new List<KeyValuePair<string, object>>()
            {
                new KeyValuePair<string, object>("@regId", regId)
            });

            ExecuteNonQuery(command);
        }
    }
}
