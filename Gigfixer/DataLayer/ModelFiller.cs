﻿using ClassLibary;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace GigFixer.DataLayer
{
    public class ModelFiller
    {
        public static List<Artist> FillArtist(SqlCommand command)
        {
            List<Artist> artists = new List<Artist>();

            using (command)
            {
                try
                {
                    command.Connection.Open();

                    SqlDataReader reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        if (reader.HasRows)
                        {
                            artists.Add(new Artist(
                                Convert.ToInt32(reader["id"]),
                                Convert.ToString(reader["email"]),
                                Convert.ToString(reader["firstName"]),
                                Convert.ToString(reader["lastName"]),
                                Convert.ToString(reader["infix"]),
                                Convert.ToString(reader["telNr"]),
                                Convert.ToInt32(reader["artistId"]),
                                Convert.ToString(reader["artistname"]),
                                Convert.ToString(reader["genre"])));
                        }
                    }

                    command.Connection.Close();

                }
                catch (Exception ex)
                {
                    Console.Write("Error: " + ex.Message);
                }

                return artists;
            }
        }

        public static bool Returntype(SqlCommand command)
        {
            bool returntype = false;

            using (command)
            {
                try
                {
                    command.Connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    if (reader.HasRows)
                    {
                        returntype = true;
                        command.Connection.Close();
                    }
                    else
                    {
                        command.Connection.Close();
                    }
                }
                catch (Exception ex)
                {
                    Console.Write("Error: " + ex.Message);
                }

                return returntype;
            }
        }

        public static List<Gig> FillGigs(SqlCommand command)
        {
            List<Gig> gigs = new List<Gig>();
            List<Hall> halls = new List<Hall>();

            using (command)
            {
                try
                {
                    command.Connection.Open();
                    SqlDataReader reader = command.ExecuteReader();

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            gigs.Add(new Gig(
                                Convert.ToInt32(reader["gigId"]),
                                new Venue(
                                    Convert.ToInt32(reader["userId"]),
                                    Convert.ToString(reader["email"]),
                                    Convert.ToString(reader["firstName"]),
                                    Convert.ToString(reader["infix"]),
                                    Convert.ToString(reader["lastName"]),
                                    Convert.ToString(reader["telNr"]),
                                    Convert.ToInt32(reader["venueId"]),
                                    Convert.ToString(reader["venuename"]),
                                    Convert.ToString(reader["city"]),
                                    Convert.ToString(reader["postalcode"]),
                                    Convert.ToString(reader["address"]),
                                    halls = new List<Hall> { new Hall(
                                    Convert.ToInt32(reader["hallId"]),
                                    Convert.ToInt32(reader["venueId"]),
                                    Convert.ToString(reader["hallname"]),
                                    Convert.ToInt32(reader["capacity"]))
                                    }
                                        ),
                                new Hall(
                                    Convert.ToInt32(reader["hallId"]),
                                    Convert.ToInt32(reader["venueId"]),
                                    Convert.ToString(reader["hallname"]),
                                    Convert.ToInt32(reader["capacity"])
                                    ),
                                Convert.ToString(reader["name"]),
                                Convert.ToString(reader["genre"]),
                                Convert.ToString(reader["gigDesc"]),
                                Convert.ToInt32(reader["wage"]),
                                Convert.ToDateTime(reader["date"])
                                                 ));
                        }
                    }
                    command.Connection.Close();
                }
                catch (Exception ex)
                {
                    Console.Write("Error: " + ex.Message);
                }

                return gigs;
            }
        }

        public static Gig FillGig(SqlCommand command)
        {
            Gig gig = null;
            List<Hall> halls = new List<Hall>();

            using (command)
            {
                try
                {
                    command.Connection.Open();

                    SqlDataReader reader = command.ExecuteReader();

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            gig = new Gig(
                                Convert.ToInt32(reader["gigId"]),
                                new Venue(
                                    Convert.ToInt32(reader["userId"]),
                                    Convert.ToString(reader["email"]),
                                    Convert.ToString(reader["firstName"]),
                                    Convert.ToString(reader["infix"]),
                                    Convert.ToString(reader["lastName"]),
                                    Convert.ToString(reader["telNr"]),
                                    Convert.ToInt32(reader["venueId"]),
                                    Convert.ToString(reader["venuename"]),
                                    Convert.ToString(reader["city"]),
                                    Convert.ToString(reader["postalcode"]),
                                    Convert.ToString(reader["address"]),
                                    halls = new List<Hall> { new Hall(
                                    Convert.ToInt32(reader["hallId"]),
                                    Convert.ToInt32(reader["venueId"]),
                                    Convert.ToString(reader["hallname"]),
                                    Convert.ToInt32(reader["capacity"]))
                                    }
                                        ),
                                new Hall(
                                    Convert.ToInt32(reader["hallId"]),
                                    Convert.ToInt32(reader["venueId"]),
                                    Convert.ToString(reader["hallname"]),
                                    Convert.ToInt32(reader["capacity"])
                                    ),
                                Convert.ToString(reader["name"]),
                                Convert.ToString(reader["genre"]),
                                Convert.ToString(reader["gigDesc"]),
                                Convert.ToInt32(reader["wage"]),
                                Convert.ToDateTime(reader["date"])
                                                 );
                        }
                    }
                    command.Connection.Close();
                }
                catch (Exception ex)
                {
                    Console.Write("Error: " + ex.Message);
                }

                return gig;
            }
        }

        public static List<Registration> FillRegistrations(SqlCommand command)
        {
            List<Registration> registrations = new List<Registration>();

            using (command)
            {
                try
                {
                    command.Connection.Open();

                    SqlDataReader reader = command.ExecuteReader();

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            registrations.Add(new Registration(
                                   Convert.ToInt32(reader["regId"]),
                                   new Artist(
                                       Convert.ToInt32(reader["userId"]),
                                       Convert.ToString(reader["email"]),
                                       Convert.ToString(reader["firstName"]),
                                       Convert.ToString(reader["lastName"]),
                                       Convert.ToString(reader["infix"]),
                                       Convert.ToString(reader["telNr"]),
                                       Convert.ToInt32(reader["artistId"]),
                                       Convert.ToString(reader["artistName"]),
                                       Convert.ToString(reader["artistGenre"])
                                       ),
                                   new Gig(
                                       Convert.ToInt32(reader["gigId"]),
                                       new Venue(
                                           0,
                                           null,
                                           null,
                                           null,
                                           null,
                                           null,
                                           Convert.ToInt32(reader["venueId"]),
                                           Convert.ToString(reader["venuename"]),
                                           Convert.ToString(reader["city"]),
                                           Convert.ToString(reader["postalcode"]),
                                           Convert.ToString(reader["address"]),
                                           null),
                                       new Hall(
                                           Convert.ToInt32(reader["hallId"]),
                                           Convert.ToInt32(reader["venueId"]),
                                           Convert.ToString(reader["hallname"]),
                                           Convert.ToInt32(reader["capacity"])
                                           ),
                                       Convert.ToString(reader["gigName"]),
                                       Convert.ToString(reader["gigGenre"]),
                                       Convert.ToString(reader["gigDesc"]),
                                       Convert.ToInt32(reader["wage"]),
                                       Convert.ToDateTime(reader["date"])
                                           ),
                                   Convert.ToString(reader["regDesc"]),
                                   Convert.ToBoolean(reader["approved"])));
                        }
                    }
                    command.Connection.Close();
                }
                catch (Exception ex)
                {
                    Console.Write("Error: " + ex.Message);
                }
                return registrations;
            }
        }

        public static Venue FillVenue(SqlCommand command)
        {
            Venue venue;
            venue = null;
            List<Hall> halls = new List<Hall>();

            int userId = 0;
            string email = "";
            string firstName = "";
            string lastName = "";
            string infix = "";
            string telNr = "";
            int venueId = 0;
            string venueName = "";
            string city = "";
            string postalcode = "";
            string address = "";

            try
            {
                command.Connection.Open();

                SqlDataReader reader = command.ExecuteReader();

                //store the data
                while (reader.Read())
                {
                    //halls

                    halls.Add(new Hall(
                        Convert.ToInt32(reader["hallId"]),
                        Convert.ToInt32(reader["venueId"]),
                        Convert.ToString(reader["name"]),
                        Convert.ToInt32(reader["capacity"])));

                    //User

                    userId = Convert.ToInt32(reader["id"]);
                    email = Convert.ToString(reader["email"]);
                    firstName = Convert.ToString(reader["firstName"]);
                    lastName = Convert.ToString(reader["lastName"]);
                    infix = Convert.ToString(reader["infix"]);
                    telNr = Convert.ToString(reader["telNr"]);

                    //Venue

                    venueId = Convert.ToInt32(reader["venueId"]);
                    venueName = Convert.ToString(reader["venuename"]);
                    city = Convert.ToString(reader["city"]);
                    postalcode = Convert.ToString(reader["postalcode"]);
                    address = Convert.ToString(reader["address"]);
                }
                command.Connection.Close();
            }
            catch (Exception ex)
            {
                Console.Write("Error: " + ex.Message);
            }


            //create venue object
            venue = new Venue(
                        userId,
                        email,
                        firstName,
                        lastName,
                        infix,
                        telNr,
                        venueId,
                        venueName,
                        city,
                        postalcode,
                        address,
                        halls);

            return venue;
        }
    }
}