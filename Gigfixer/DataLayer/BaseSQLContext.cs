﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace GigFixer.DataLayer
{
    public class BaseSQLContext
    {
        private readonly string conn = "Server=mssql.fhict.local;Database=dbi407381;User Id=dbi407381;Password=Password1234;";
        internal void ExecuteNonQuery(SqlCommand command)
        {
            using (command)
            {
                try
                {
                    command.Connection.Open();
                    command.ExecuteNonQuery();
                    command.Connection.Close();
                }
                catch (Exception ex)
                {
                    Console.Write("Error: " + ex.Message);
                }

            }
        }

        internal SqlCommand CreateSQLCommand(string query)
        {
            SqlCommand command = new SqlCommand
            {
                Connection = new SqlConnection(conn),
                CommandText = query
            };

            return command;
        }

        internal SqlCommand CreateSQLCommand(string query, List<KeyValuePair<string, object>> parameters)
        {
            SqlCommand command = new SqlCommand
            {
                Connection = new SqlConnection(conn),
                CommandText = query
            };

            foreach (KeyValuePair<string, object> kvp in parameters)
            {
                command.Parameters.AddWithValue(kvp.Key, kvp.Value);
            }

            return command;
        }
    }
}
