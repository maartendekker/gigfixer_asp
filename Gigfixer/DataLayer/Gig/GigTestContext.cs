﻿using ClassLibary;
using GigFixer.DataLayer.Event;
using GigFixer.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GigFixer.DataLayer
{
    public class GigTestContext : IGigContext
    {
        List<Gig> gigs = new List<Gig>();
        public void AddGig(Gig gig)
        {
            gigs.Add(gig);
        }

        public void DeleteGig(int gigid)
        {
            Gig gig = gigs.Find(f => f.Id == 5);
            gigs.Remove(gig);
        }

        public List<Gig> GetGig()
        {
            return gigs;
        }

        public Gig GetGigByGigId(int gigid)
        {
            Gig gig = new Gig(5,
                              null,
                              null,
                              "JelleFest",
                              "Metal",
                              "Het laatste weekend van de vrijbuiter",
                              50,
                              DateTime.Today);

            gigs.Add(gig);

            return gigs.Find(f => f.Id == gigid);

        }

        public List<Gig> GetGigForVenue(int venueId)
        {
            throw new NotImplementedException();
        }

        public int GetRegisterdCount(int artistId)
        {
            throw new NotImplementedException();
        }

        public List<Gig> GetRegisterdGigs(int artistId)
        {
            throw new NotImplementedException();
        }
    }
}
