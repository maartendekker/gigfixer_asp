﻿using ClassLibary;
using GigFixer.DataLayer.Event;
using GigFixer.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace GigFixer.DataLayer
{
    public class GigSQLContext : BaseSQLContext, IGigContext
    {
        public void AddGig(Gig gig)
        {
            string query = "Addgig";
            SqlCommand command = CreateSQLCommand(query, new List<KeyValuePair<string, object>>()
            {
                new KeyValuePair<string, object>("@venueId", gig.Venue.VenueId),
                new KeyValuePair<string, object>("@hallId", gig.Hall.Id),
                new KeyValuePair<string, object>("@name", gig.Name),
                new KeyValuePair<string, object>("@genre", gig.Genre),
                new KeyValuePair<string, object>("@description", gig.Description),
                new KeyValuePair<string, object>("@wage", gig.Wage),
                new KeyValuePair<string, object>("@date", gig.Date)
            });

            command.CommandType = CommandType.StoredProcedure;
            ExecuteNonQuery(command);
        }

        public void DeleteGig(int gigId)
        {
            string query = "DELETE FROM Gigfixer.[Gig] WHERE id = @gigId";

            SqlCommand command = CreateSQLCommand(query, new List<KeyValuePair<string, object>>()
            {
                new KeyValuePair<string, object>("@gigId", gigId)
            });

            ExecuteNonQuery(command);
        }

        public List<Gig> GetGig()
        {
            string query = "SELECT " +
            "Gigfixer.[Gig].id as gigId, " +
            "Gigfixer.[Gig].venueId as venueId, " +
            "Gigfixer.[Gig].hallId as hallId, " +
            "Gigfixer.[Gig].name as name, " +
            "Gigfixer.[Gig].genre as genre, " +
            "Gigfixer.[Gig].description as gigDesc, " +
            "Gigfixer.[Gig].wage as wage, " +
            "Gigfixer.[Gig].date as date, " +
            "Gigfixer.[Venue].userId as userId, " +
            "Gigfixer.[Venue].name as venuename, " +
            "Gigfixer.[Venue].city as city, " +
            "Gigfixer.[Venue].address as address, " +
            "Gigfixer.[Venue].postalcode as postalcode, " +
            "Gigfixer.[User].email as email, " +
            "Gigfixer.[User].firstName as firstName, " +
            "Gigfixer.[User].infix as infix, " +
            "Gigfixer.[User].lastName as lastName, " +
            "Gigfixer.[User].telNr as telNr, " +
            "Gigfixer.[Hall].name as hallname, " +
            "Gigfixer.[Hall].capacity as capacity " +
            "FROM Gigfixer.[Gig] " +
            "INNER JOIN Gigfixer.[Venue] on Gigfixer.[Venue].id = Gigfixer.[Gig].venueId " +
            "INNER JOIN Gigfixer.[User] on Gigfixer.[User].id = Gigfixer.[Venue].userId " +
            "INNER JOIN Gigfixer.[Hall] on Gigfixer.[Hall].id = Gigfixer.[Gig].hallId";

            SqlCommand command = CreateSQLCommand(query);

            List<Gig> gigs = new List<Gig>();

            gigs = ModelFiller.FillGigs(command);

            return gigs;
        }

        public Gig GetGigByGigId(int gigid)
        {
            string query = "SELECT " +
            "Gigfixer.[Gig].id as gigId, " +
            "Gigfixer.[Gig].venueId as venueId, " +
            "Gigfixer.[Gig].hallId as hallId, " +
            "Gigfixer.[Gig].name as name, " +
            "Gigfixer.[Gig].genre as genre, " +
            "Gigfixer.[Gig].description as gigDesc, " +
            "Gigfixer.[Gig].wage as wage, " +
            "Gigfixer.[Gig].date as date, " +
            "Gigfixer.[Venue].userId as userId, " +
            "Gigfixer.[Venue].name as venuename, " +
            "Gigfixer.[Venue].city as city, " +
            "Gigfixer.[Venue].address as address, " +
            "Gigfixer.[Venue].postalcode as postalcode, " +
            "Gigfixer.[User].email as email, " +
            "Gigfixer.[User].firstName as firstName, " +
            "Gigfixer.[User].infix as infix, " +
            "Gigfixer.[User].lastName as lastName, " +
            "Gigfixer.[User].telNr as telNr, " +
            "Gigfixer.[Hall].name as hallname, " +
            "Gigfixer.[Hall].capacity as capacity " +
            "FROM Gigfixer.[Gig] " +
            "INNER JOIN Gigfixer.[Venue] on Gigfixer.[Venue].id = Gigfixer.[Gig].venueId " +
            "INNER JOIN Gigfixer.[User] on Gigfixer.[User].id = Gigfixer.[Venue].userId " +
            "INNER JOIN Gigfixer.[Hall] on Gigfixer.[Hall].id = Gigfixer.[Gig].hallId " +
            "WHERE Gigfixer.[Gig].id = @gigid";

            SqlCommand command = CreateSQLCommand(query, new List<KeyValuePair<string, object>>()
            {
                new KeyValuePair<string, object>("@gigid", gigid)
            });


            List<Hall> halls = new List<Hall>();
            Gig gig = null;

            gig = ModelFiller.FillGig(command);

            return gig;
        }

        public List<Gig> GetGigForVenue(int venueId)
        {
            string query = "SELECT " +
            "Gigfixer.[Gig].id as gigId, " +
            "Gigfixer.[Gig].venueId as venueId, " +
            "Gigfixer.[Gig].hallId as hallId, " +
            "Gigfixer.[Gig].name as name, " +
            "Gigfixer.[Gig].genre as genre, " +
            "Gigfixer.[Gig].description as gigDesc, " +
            "Gigfixer.[Gig].wage as wage, " +
            "Gigfixer.[Gig].date as date, " +
            "Gigfixer.[Venue].userId as userId, " +
            "Gigfixer.[Venue].name as venuename, " +
            "Gigfixer.[Venue].city as city, " +
            "Gigfixer.[Venue].address as address, " +
            "Gigfixer.[Venue].postalcode as postalcode, " +
            "Gigfixer.[User].email as email, " +
            "Gigfixer.[User].firstName as firstName, " +
            "Gigfixer.[User].infix as infix, " +
            "Gigfixer.[User].lastName as lastName, " +
            "Gigfixer.[User].telNr as telNr, " +
            "Gigfixer.[Hall].name as hallname, " +
            "Gigfixer.[Hall].capacity as capacity " +
            "FROM Gigfixer.[Gig] " +
            "INNER JOIN Gigfixer.[Venue] on Gigfixer.[Venue].id = Gigfixer.[Gig].venueId " +
            "INNER JOIN Gigfixer.[User] on Gigfixer.[User].id = Gigfixer.[Venue].userId " +
            "INNER JOIN Gigfixer.[Hall] on Gigfixer.[Hall].id = Gigfixer.[Gig].hallId " +
            "WHERE Gigfixer.[Gig].venueId = @venueId";

            SqlCommand command = CreateSQLCommand(query, new List<KeyValuePair<string, object>>()
            {
                new KeyValuePair<string, object>("@venueId", venueId)
            });

            List<Gig> gigs = new List<Gig>();

            gigs = ModelFiller.FillGigs(command);

            return gigs;
        }

        public int GetRegisterdCount(int artistId)
        {
            string query = "SELECT " +
            "COUNT(Gigfixer.[Gig].id) as numberofregistrations " +
            "FROM Gigfixer.[Gig] " +
            "INNER JOIN Gigfixer.[Registration] on Gigfixer.[Registration].gigId = Gigfixer.[Gig].Id " +
            "WHERE Gigfixer.[Registration].artistId = @artistId ";

            SqlCommand command = CreateSQLCommand(query, new List<KeyValuePair<string, object>>()
            {
                new KeyValuePair<string, object>("@artistId", artistId)
            });

            int count = new int();

            using (command)
            {
                try
                {
                    command.Connection.Open();

                    SqlDataReader reader = command.ExecuteReader();

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            count = Convert.ToInt32(reader["numberofregistrations"]);
                        }
                    }
                    command.Connection.Close();
                }
                catch (Exception ex)
                {
                    Console.Write("Error: " + ex.Message);
                }

            }
            return count;
        }

        public List<Gig> GetRegisterdGigs(int artistid)
        {
            string query = "SELECT DISTINCT " +
            "Gigfixer.[Gig].id as gigId, " +
            "Gigfixer.[Gig].venueId as venueId, " +
            "Gigfixer.[Gig].hallId as hallId, " +
            "Gigfixer.[Gig].name as name, " +
            "Gigfixer.[Gig].genre as genre, " +
            "Gigfixer.[Gig].description as gigDesc, " +
            "Gigfixer.[Gig].wage as wage, " +
            "Gigfixer.[Gig].date as date, " +
            "Gigfixer.[Venue].userId as userId, " +
            "Gigfixer.[Venue].name as venuename, " +
            "Gigfixer.[Venue].city as city, " +
            "Gigfixer.[Venue].address as address, " +
            "Gigfixer.[Venue].postalcode as postalcode, " +
            "Gigfixer.[User].email as email, " +
            "Gigfixer.[User].firstName as firstName, " +
            "Gigfixer.[User].infix as infix, " +
            "Gigfixer.[User].lastName as lastName, " +
            "Gigfixer.[User].telNr as telNr, " +
            "Gigfixer.[Hall].name as hallname, " +
            "Gigfixer.[Hall].capacity as capacity " +
            "FROM Gigfixer.[Gig] " +
            "INNER JOIN Gigfixer.[Registration] on Gigfixer.[Registration].gigId = Gigfixer.[Gig].id " +
            "INNER JOIN Gigfixer.[Venue] on Gigfixer.[Venue].id = Gigfixer.[Gig].venueId " +
            "INNER JOIN Gigfixer.[User] on Gigfixer.[User].id = Gigfixer.[Venue].userId " +
            "INNER JOIN Gigfixer.[Hall] on Gigfixer.[Hall].id = Gigfixer.[Gig].hallId " +
            "WHERE Gigfixer.[Registration].artistId = @artistId ";

            SqlCommand command = CreateSQLCommand(query, new List<KeyValuePair<string, object>>()
            {
                new KeyValuePair<string, object>("@artistId", artistid)
            });

            List<Gig> gigs = new List<Gig>();

            gigs = ModelFiller.FillGigs(command);

            return gigs;
        }
    }
}
