﻿using ClassLibary;
using GigFixer.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GigFixer.DataLayer.Event
{
    public interface IGigContext
    {
        void AddGig(Gig gig);
        List<Gig> GetGig();
        Gig GetGigByGigId(int gigid);
        List<Gig> GetGigForVenue(int venueId);
        int GetRegisterdCount(int artistId);
        List<Gig> GetRegisterdGigs(int artistId);
        void DeleteGig(int gigid);
    }
}
