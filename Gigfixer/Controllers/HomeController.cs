﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using GigFixer.DataLayer;
using GigFixer.Repositories;
using ClassLibary;
using GigFixer.ViewModels;

namespace GigFixer.Controllers
{
    public class HomeController : AccountController
    {
        public IActionResult Home() //redirects to homepage
        {
            //checks if user is logged in
            base.CheckForLogin();

            //check the accounttype based on accountId cookie, and redirects properly
            AccountRepository accountRepository = new AccountRepository(new AccountSQLContext());

            if (accountRepository.IsArtist(Convert.ToInt32(Request.Cookies["accountId"])) == true)
            {
                return RedirectToAction("ArtistHome", "Home");
            }
            else
            {
                return RedirectToAction("VenueHome", "Home");
            }
        }

        public IActionResult ArtistHome()
        {
            //checks if user is logged in
            base.CheckForLogin();

            //instantiate repository and viewmodel
            ArtistRepository artistRepository = new ArtistRepository(new ArtistSQLContext());
            ArtistViewModel artistViewModel = new ArtistViewModel();

            //filling model and viewmodel
            Artist artist = artistRepository.GetArtistByAccountId(Convert.ToInt32(Request.Cookies["accountId"]));
            artistViewModel = artistViewModel.AVM(artist);
            
            //redirect
            return View("ArtistHome", artistViewModel);
        }
        
        public IActionResult VenueHome()
        {
            //checks if user is logged in
            base.CheckForLogin();

            //instantiate repository
            VenueRepository venueRepository = new VenueRepository(new VenueSQLContext());

            //binding data
            Venue venue = venueRepository.GetVenueByAccountId(Convert.ToInt32(Request.Cookies["accountId"]));
            VenueViewModel venueViewModel = new VenueViewModel();
            venueViewModel = venueViewModel.VVM(venue);

            //redirect
            return View("VenueHome", venueViewModel);
        }

        public IActionResult Login()
        {
            return View();
        }
    }
}
