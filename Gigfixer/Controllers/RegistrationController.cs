﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GigFixer.DataLayer;
using GigFixer.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace GigFixer.Controllers
{
    public class RegistrationController : Controller
    {
        public IActionResult RegistrationPage() //redirects to registrationpage based on accounttype (accountId cookie)
        {
            AccountRepository accountRepository = new AccountRepository(new AccountSQLContext());
            
            if (accountRepository.IsArtist(Convert.ToInt32(Request.Cookies["accountId"])) == true)
            {
                return RedirectToAction("Registration", "Artist");
            }
            else
            {
                return RedirectToAction("Registration", "Venue");
            }
        }

        [HttpPost]
        public IActionResult DeleteRegistration(int regId) //deletes a registration
        {
            RegistrationRepository registrationRepository = new RegistrationRepository(new RegistrationSQLContext());

            registrationRepository.DeleteRegistration(regId);

            TempData["Status"] = ("Deleted registration successfully");

            return RedirectToAction("RegistrationPage", "Registration");
        }

        [HttpPost]
        public IActionResult Approve(int regId) //approves a registration
        {
            RegistrationRepository registrationRepository = new RegistrationRepository(new RegistrationSQLContext());

            registrationRepository.ApproveRegistration(regId);

            TempData["Status"] = ("Approved registration successfully");

            return RedirectToAction("RegistrationPage", "Registration");
        }

        [HttpPost]
        public IActionResult Decline(int regId) //declines a registration
        {
            RegistrationRepository registrationRepository = new RegistrationRepository(new RegistrationSQLContext());

            registrationRepository.DeclineRegistration(regId);

            TempData["Status"] = ("Declined registration successfully");

            return RedirectToAction("RegistrationPage", "Registration");
        }


    }
}