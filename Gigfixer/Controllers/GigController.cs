﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ClassLibary;
using GigFixer.DataLayer;
using GigFixer.ViewModels;
using GigFixer.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace GigFixer.Controllers
{
    public class GigController : AccountController
    {
        [HttpGet]
        public IActionResult GigPage() //redirects to the gigpage depending on account type
        {
            //check if the user is logged in
            base.CheckForLogin();

            //instantiate repositories and give them a context
            AccountRepository accountRepository = new AccountRepository(new AccountSQLContext());
            GigRepository gigRepository = new GigRepository(new GigSQLContext());
            
            //checks for account type and redirects appropriatly
            if (accountRepository.IsArtist(Convert.ToInt32(Request.Cookies["accountId"])) == true)
            {
                return RedirectToAction("GigPage", "Artist");
            }
            else
            {
                return RedirectToAction("GigPage", "Venue");
            }
        }

        [HttpPost]
        public IActionResult DeleteGig(int gigid) //deletes a gig based on gigId
        {
            //check if user is logged in
            base.CheckForLogin();

            //instantiate repository and give it a context
            GigRepository gigRepository = new GigRepository(new GigSQLContext());

            //execute method
            gigRepository.DeleteGig(gigid);

            //redirect
            TempData["Status"] = "Successfully deleted the gig";
            return RedirectToAction("GigPage", "Gig");
        }

        [HttpPost]
        public IActionResult Create(GigViewModel gigViewModel) //creates a gig
        {
            //check if user is logged in
            base.CheckForLogin();

            //instantiate repositories and give them a context
            GigRepository gigRepository = new GigRepository(new GigSQLContext());
            VenueRepository venueRepository = new VenueRepository(new VenueSQLContext());

            //filling the models
            Venue venue = venueRepository.GetVenueByAccountId(Convert.ToInt32(Request.Cookies["accountId"]));
            Hall hall = venueRepository.GetHallByHallId(gigViewModel.HallId);
            Gig gig = new Gig(0, venue, hall, gigViewModel.Name, gigViewModel.Genre, gigViewModel.Description, gigViewModel.Wage, gigViewModel.Date);

            //executing method
            gigRepository.AddGig(gig);

            //redirect
            TempData["Status"] = "Successfully created the gig";
            return RedirectToAction("GigPage", "Venue");
        }

        [HttpPost]
        public IActionResult RegisterPage(int gigid) //shows the page where artists can register on a gig.
        {
            //check if user is logged in
            base.CheckForLogin();

            //instantiate repository and fill the gig based on gigId
            GigRepository gigRepository = new GigRepository(new GigSQLContext());
            Gig gig = gigRepository.GetGigByGigId(gigid);
            
            //create viewmodels
            RegisterViewModel registerViewModel = new RegisterViewModel();
            GigViewModel gigViewModel = new GigViewModel();

            //binding data
            registerViewModel.GigViewModel = gigViewModel.GVM(gig);

            //redirect
            return View("RegisterPage", registerViewModel);
        }

        [HttpPost]
        public IActionResult Register(RegisterViewModel registerViewModel, int gigid) //saves the registration
        {
            base.CheckForLogin();
            TempData["Status"] = null;

            //instantiate repositories and give them a context
            ArtistRepository artistRepository = new ArtistRepository(new ArtistSQLContext());
            GigRepository gigRepository = new GigRepository(new GigSQLContext());
            RegistrationRepository registrationRepository = new RegistrationRepository(new RegistrationSQLContext());

            //no registrion is approved on initial submission
            registerViewModel.Approved = false;

            //filling the models
            Artist artist = artistRepository.GetArtistByAccountId(Convert.ToInt32(Request.Cookies["accountId"]));
            Gig gig = gigRepository.GetGigByGigId(gigid);
            Registration registration = new Registration(0, artist, gig, registerViewModel.Description, registerViewModel.Approved);

            if (registrationRepository.GetRegistrationForArtist(artist.ArtistId).Any(f => f.Gig.Id == gig.Id))
            {
                TempData["Status"] = "you already registerd on this gig";
                return RedirectToAction("GigPage", "Artist");
            }
            else
            {
                TempData["Status"] = ("You successfully registerd on to " + gig.Name);

                //executing the actual registration method
                registrationRepository.Register(registration);

                //redirect to gigpage
                return RedirectToAction("GigPage", "Artist");
            }

        }
    }
}