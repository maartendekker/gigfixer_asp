﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ClassLibary;
using GigFixer.DataLayer;
using GigFixer.ViewModels;
using GigFixer.Repositories;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace GigFixer.Controllers
{
    public class AccountController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        //checks for valid login and redirects on basis of the account type
        [HttpPost]
        public IActionResult Login(AccountViewModel viewModel)
        {
            //instantiate repository
            AccountRepository accountRepository = new AccountRepository(new AccountSQLContext());

            int? loginid;

            //Client-side form validation
            if (ModelState.IsValid == true)
            {
                loginid = accountRepository.CheckValidLogin(viewModel.Email, viewModel.Password);

                //if the user logged in correctly, redirect appropriatly
                if (loginid != null)
                {
                    Response.Cookies.Append("accountId", Convert.ToString(loginid), new CookieOptions { Expires = DateTime.Now.AddDays(1) });

                    if (accountRepository.IsArtist(loginid) == true)
                    {
                        return RedirectToAction("ArtistHome", "Home");
                    }
                    else
                    {
                        return RedirectToAction("VenueHome", "Home");
                    }
                }
            }

            //if the user did not login correctly, add error and redirect
            ModelState.AddModelError("ErrorMessage", "Invald login credentials");
            return View();
        }

        public void CheckForLogin() // checks if accountId cookie has a value, otherwise redirect to loginscreen

        {
            if (Convert.ToInt32(Request.Cookies["accountId"]) != 0)
            {
                return;
            }

            Response.Redirect("Login");

        }

        [HttpGet]
        public IActionResult Choice() //shows the create account choice page
        {
            return View();
        }

        [HttpGet]
        public IActionResult RegisterVenue() //shows the registervenue page
        {
            return View("RegisterVenue");
        }

        [HttpPost]
        public IActionResult RegisterVenue(AccountViewModel viewModel) //registers a venue account
        {
            if (ModelState.IsValid)
            {
                //instantiate repository and give it a context
                AccountRepository accountRepository = new AccountRepository(new AccountSQLContext());

                //fill models
                List<Hall> halls = new List<Hall>
                {
                    new Hall(0, 0, viewModel.Hallname, viewModel.Hallcapacity)
                };

                Venue venue = new Venue
                    (0,
                    viewModel.Email,
                    viewModel.Firstname,
                    viewModel.Lastname,
                    viewModel.Infix,
                    viewModel.TelNr,
                    0,
                    viewModel.Venuename,
                    viewModel.City,
                    viewModel.Postalcode,
                    viewModel.Address,
                    halls);

                string password = viewModel.Password;

                //execute registervenue method
                accountRepository.RegisterVenue(venue, password);

                //redirect
                return RedirectToAction("Login", "Home");
            }

            return View();
        }

        [HttpGet]
        public IActionResult RegisterArtist() //shows the registerartist page
        {
            return View("RegisterArtist");
        }


        [HttpPost]
        public IActionResult RegisterArtist(AccountViewModel viewModel) //registers an artist
        {
            if (ModelState.IsValid)
            {
                //instantiate repository and give it a context
                AccountRepository accountRepository = new AccountRepository(new AccountSQLContext());

                //filling models
                Artist artist = new Artist(0, viewModel.Email, viewModel.Firstname, viewModel.Lastname, viewModel.Infix, viewModel.TelNr, 0, viewModel.Artistname, viewModel.Genre);
                string password = viewModel.Password;

                //execute registerartist method
                accountRepository.RegisterArtist(artist, password);

                //redirect
                return RedirectToAction("Login", "Home");
            }

            return View();
        }

        [HttpGet]
        public IActionResult Logout() //deletes the cookie and redirects to loginpage
        {
            Response.Cookies.Delete("accountId");
            return RedirectToAction("Login", "Home");
        }
    }
}

