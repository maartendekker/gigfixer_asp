﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GigFixer.DataLayer;
using GigFixer.ViewModels;
using GigFixer.Repositories;
using ClassLibary;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace GigFixer.Controllers
{
    public class ArtistController : AccountController
    {
        [HttpGet]
        public IActionResult GigPage() //shows the gigpage for artists
        {
            //check if user is logged in
            base.CheckForLogin();

            //instantiate repository and give it a context
            GigRepository gigRepository = new GigRepository(new GigSQLContext());
           
            //filling viewbag with list of all gigs
            List<Gig> gigs = gigRepository.GetGig();
            ViewBag.ListGigs = gigs.OrderBy(f => f.Date);

            //redirect
            return View("GigPage");
        }

        [HttpGet]
        public IActionResult Registration() //shows the page where artists can register on gigs
        {
            //checks if user is logged in
            base.CheckForLogin();

            ArtistRepository artistRepository = new ArtistRepository(new ArtistSQLContext());

            //filling the artistViewModel
            Artist artist = artistRepository.GetArtistByAccountId(Convert.ToInt32(Request.Cookies["accountId"]));
            ArtistViewModel artistViewModel = new ArtistViewModel();
            artistViewModel = artistViewModel.AVM(artist);

            //Getting List of gigs and it's count
            GigRepository gigRepository = new GigRepository(new GigSQLContext());

            int count = gigRepository.GetRegisterdCount(artist.ArtistId);
            List<Gig> gigs = gigRepository.GetRegisterdGigs(artist.ArtistId);
            gigs.OrderBy(f => f.Date);

            ViewBag.ListGigs = gigs;
            ViewBag.Count = count;

            //Getting List of Registrations
            RegistrationRepository registrationRepository = new RegistrationRepository(new RegistrationSQLContext());

            List<Registration> list = new List<Registration>();
            list = registrationRepository.GetRegistrationForArtist(artist.ArtistId);
            ViewBag.ListRegistrations = list;
            
            return View("Registration", artistViewModel);
        }
    }
}
