﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GigFixer.DataLayer;
using GigFixer.ViewModels;
using GigFixer.Repositories;
using ClassLibary;
using Microsoft.AspNetCore.Mvc;

namespace GigFixer.Controllers
{
    public class VenueController : AccountController
    {
        [HttpGet]
        public IActionResult GigPage()
        {
            //check if user is logged in
            base.CheckForLogin();

            //instantiate repositories and give them a context
            GigRepository gigRepository = new GigRepository(new GigSQLContext());
            VenueRepository venueRepository = new VenueRepository(new VenueSQLContext());

            //create viewmodel
            VenueViewModel venueViewModel = new VenueViewModel();

            //filling models, viewmodels and viewbags
            Venue venue = venueRepository.GetVenueByAccountId(Convert.ToInt32(Request.Cookies["accountId"]));
            venueViewModel = venueViewModel.VVM(venue);
            ViewBag.ListGigs = gigRepository.GetGigForVenue(venue.VenueId).OrderBy(f => f.Date);

            //redirect
            return View("GigPage", venueViewModel);
        }

        [HttpGet]
        public IActionResult CreateGig()
        {
            //check if user is logged in
            base.CheckForLogin();

            //instantiate repository
            VenueRepository venueRepository = new VenueRepository(new VenueSQLContext());

            //binding data
            Venue venue = venueRepository.GetVenueByAccountId(Convert.ToInt32(Request.Cookies["accountId"]));
            ViewBag.ListHalls = (List<Hall>)venue.Halls;

            //redirect
            return View("CreateGig");
        }

        [HttpGet]
        public IActionResult Registration()
        {
            //check if user is logged in
            base.CheckForLogin();

            //initialising repositories
            VenueRepository venueRepository = new VenueRepository(new VenueSQLContext());
            GigRepository gigRepository = new GigRepository(new GigSQLContext());
            RegistrationRepository registrationRepository = new RegistrationRepository(new RegistrationSQLContext());

            //getting venue object
            Venue venue = venueRepository.GetVenueByAccountId(Convert.ToInt32(Request.Cookies["accountId"]));

            //initialising venueViewModel and filling it
            VenueViewModel venueViewModel = new VenueViewModel();
            venueViewModel = venueViewModel.VVM(venue);

            //filling viewbags
            ViewBag.ListGigs = gigRepository.GetGigForVenue(venue.VenueId).OrderBy(f => f.Date);
            ViewBag.ListRegistrations = registrationRepository.GetRegistrationForVenue(venue.VenueId);

            return View("Registration", venueViewModel);
        }

        public IActionResult Hall()
        {
            //checks if user is logged in
            base.CheckForLogin();

            //instantiate repository
            VenueRepository venueRepository = new VenueRepository(new VenueSQLContext());

            //binding data
            Venue venue = venueRepository.GetVenueByAccountId(Convert.ToInt32(Request.Cookies["accountId"]));
            VenueViewModel venueViewModel = new VenueViewModel();
            venueViewModel = venueViewModel.VVM(venue);

            //redirect
            return View("Hall", venueViewModel);
        }


        public IActionResult CreateHallPage() //shows the page where venues can create halls
        {
            base.CheckForLogin();
            return View("CreateHall");
        }

        [HttpPost]
        public IActionResult CreateHall(HallViewModel hallViewModel) //creates a hall
        {
            //checks if the user is logged in
            base.CheckForLogin();

            //instantiate repository
            VenueRepository venueRepository = new VenueRepository(new VenueSQLContext());

            //filling models
            Venue venue = venueRepository.GetVenueByAccountId(Convert.ToInt32(Request.Cookies["accountId"]));
            Hall hall = new Hall(0, venue.VenueId, hallViewModel.Name, hallViewModel.Capacity);

            //execute method that adds the hall
            venueRepository.AddHall(hall);

            //redirect
            TempData["Status"] = ("Successfully created a hall");
            return RedirectToAction("Hall","Venue");
        }

        public IActionResult DeleteHall(int hallId) //deletes a hall
        {
            base.CheckForLogin();
            VenueRepository venueRepository = new VenueRepository(new VenueSQLContext());

            venueRepository.DeleteHall(hallId);
            TempData["Status"] = ("Successfully deleted a hall");
            return RedirectToAction("Hall", "Venue");
        }
    }
}