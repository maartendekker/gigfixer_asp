﻿using System;
using ClassLibary;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GigFixer.ViewModels
{
    public class VenueViewModel
    {
        public IReadOnlyList<Hall> Halls { get; set; }
        public int VenueId { get; set; }
        public string Name { get; set; }
        public string City { get; set; }
        public string Address { get; set; }
        public string PostalCode { get; set; }
        public AccountViewModel AccountViewModel { get; set; }

        public VenueViewModel VVM(Venue venue) //fills a VenueViewModel from a Venue
        {
            AccountViewModel accountViewModel = new AccountViewModel();

            this.Halls = venue.Halls;
            this.VenueId = venue.VenueId;
            this.Name = venue.Name;
            this.City = venue.City;
            this.Address = venue.Address;
            this.PostalCode = venue.PostalCode;
            this.AccountViewModel = accountViewModel.AVMforVenue(venue);

            return this;
        }
    }
}
