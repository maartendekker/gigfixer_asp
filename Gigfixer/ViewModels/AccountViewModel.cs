﻿using ClassLibary;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GigFixer.ViewModels
{
    public class AccountViewModel
    {
        //Id
        public int Id { get; set; }

        //Email
        [Required(ErrorMessage = "Email is required")]
        [EmailAddress(ErrorMessage = "Email needs to be a valid Email Adress")]
        [Display(Name = "Email", Prompt = "Email address")]
        public string Email { get; set; }

        //Password
        [Required(ErrorMessage = "Password is required")]
        [Display(Name = "Password", Prompt = "password")]
        public string Password { get; set; }

        //Firstname

        [Display(Name = "Firstname", Prompt = "Firstname")]
        public string Firstname { get; set; }

        //Lastname

        [Display(Name = "Lastname", Prompt = "Lastname")]
        public string Lastname { get; set; }

        //Infix

        [Display(Name = "Infix", Prompt = "infix")]
        public string Infix { get; set; }

        //Phone number
        [Display(Name = "Tel. nr", Prompt = "Phone number?")]
        public string TelNr { get; set; }

        //Artistname
        [Display(Name = "Artistname", Prompt = "Artistname")]
        public string Artistname { get; set; }

        //Genre
        [Display(Name = "Genre", Prompt = "Genre")]
        public string Genre { get; set; }

        //Venuename
        [Display(Name = "Venuename", Prompt = "Venuename")]
        public string Venuename { get; set; }

        //City
        [Display(Name = "City", Prompt = "City")]
        public string City { get; set; }

        //Postalcode
        [Display(Name = "Postalcode", Prompt = "ZIP")]
        public string Postalcode { get; set; }

        //Address
        [Display(Name = "Address", Prompt = "Street + nr")]
        public string Address { get; set; }

        //Hall proporties
        //Name
        [Display(Name = "Hall name", Prompt = "The name of your main hall")]
        public string Hallname { get; set; }

        [Display(Name = "Capacity", Prompt = "capacity of your main hall")]
        public int Hallcapacity { get; set; }

        public AccountViewModel AVMforVenue(Venue venue) //fills a AccountViewModel from a Venue
        {
            this.Id = venue.Id;
            this.Email = venue.Email;
            this.Firstname = venue.FirstName;
            this.Lastname = venue.LastName;
            this.Infix = venue.Infix;
            this.TelNr = venue.TelNr;
            return this;
        }

        public AccountViewModel AVMforArtist(Artist artist) //fills a AccountViewModel from a Artist
        {
            this.Id = artist.Id;
            this.Email = artist.Email;
            this.Firstname = artist.FirstName;
            this.Lastname = artist.LastName;
            this.Infix = artist.Infix;
            this.TelNr = artist.TelNr;
            return this;
        }
    }
}
