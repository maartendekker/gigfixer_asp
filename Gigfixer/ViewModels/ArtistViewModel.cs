﻿using ClassLibary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GigFixer.ViewModels
{
    public class ArtistViewModel
    {
        public int ArtistId { get; set; }
        public string Name { get; set; }
        public string Genre { get; set; }
        public AccountViewModel AccountViewModel { get; set; }

        public ArtistViewModel AVM(Artist artist) //fills a ArtistViewModel from a Artist
        {
            AccountViewModel accountViewModel = new AccountViewModel();

            this.ArtistId = artist.ArtistId;
            this.Name = artist.Name;
            this.Genre = artist.Genre;
            this.AccountViewModel = accountViewModel.AVMforArtist(artist);

            return this;
        }
    }
}
