﻿using ClassLibary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GigFixer.ViewModels
{
    public class GigViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public string Genre { get; set; }

        public string Description { get; set; }

        public int Wage { get; set; }

        public DateTime Date { get; set; }

        public int VenueId { get; set; }

        public int HallId { get; set; }

        public GigViewModel GVM(Gig gig) //fills a GigViewModel from a Gig
        {
            GigViewModel gigViewModel = new GigViewModel
            {
                Id = gig.Id,
                Name = gig.Name,
                Genre = gig.Genre,
                Description = gig.Description,
                Wage = gig.Wage,
                Date = gig.Date,
                VenueId = gig.Venue.Id,
                HallId = gig.Hall.Id
            };

            return gigViewModel;
        }
    }
}
