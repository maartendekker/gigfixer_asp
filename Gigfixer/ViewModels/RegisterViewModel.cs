﻿using ClassLibary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GigFixer.ViewModels
{
    public class RegisterViewModel
    {
        public int Id { get; set; }
        public GigViewModel GigViewModel { get; set; }
        public string Description { get; set; }
        public bool Approved { get; set; }

    }
}
