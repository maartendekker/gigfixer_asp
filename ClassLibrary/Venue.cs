﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibary
{
    public class Venue : User
    {
        private readonly List<Hall> halls;
        public int VenueId { get; }
        public string Name { get; }
        public string City { get; }
        public string Address { get; }
        public string PostalCode { get; }
        public IReadOnlyList<Hall> Halls
        {
            get
            {
                return this.halls;
            }
        }

        public Venue(int id, string email, string firstName, string lastName, string infix, string telNr, int venueId, string name, string city, string postalcode, string address, List<Hall> halls)
            : base(id, email, firstName, lastName, infix, telNr)
        {
            VenueId = venueId;
            Name = name;
            City = city;
            PostalCode = postalcode;
            Address = address;
            this.halls = halls;
        }



    }
}
