﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ClassLibary
{
    public class Registration
    {
        public int Id { get; }

        public Artist Artist { get; }
        public Gig Gig { get; }
        public string Description { get; }
        public bool Approved { get; }

        public Registration(int id, Artist artist, Gig gig, string description, bool approved)
        {
            this.Id = id;
            this.Artist = artist;
            this.Gig = gig;
            this.Description = description;
            this.Approved = approved;
        }
    }
}
