﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ClassLibary
{
    public class Hall
    {
        public Hall(int id, int venueId, string name, int capacity)
        {
            this.Id = id;
            this.VenueId = venueId;
            this.Name = name;
            this.Capacity = capacity;
        }

        public int Id { get; }
        public int VenueId { get; }
        public string Name { get; }
        public int Capacity { get; }
    }
}
