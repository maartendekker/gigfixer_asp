﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibary
{
    public class Artist : User
    {
        public int ArtistId { get; }
        public string Name { get; }
        public string Genre { get; }

        public Artist(int id, string email, string firstName, string lastName, string infix, string telNr, int artistId, string artistname, string genre)
            : base(id, email, firstName, lastName, infix, telNr)
        {
            ArtistId = artistId;
            Name = artistname;
            Genre = genre;
        }
    }
}
