﻿using ClassLibary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ClassLibary
{
    public class Gig
    {
        public Gig(int id, Venue venue, Hall hall, string name, string genre, string description, int wage, DateTime date)
        {
            this.Id = id;
            this.Venue = venue;
            this.Hall = hall;
            this.Name = name;
            this.Genre = genre;
            this.Description = description;
            this.Wage = wage;
            this.Date = date;
        }

        public int Id { get; }
        public Venue Venue { get; }
        public Hall Hall { get; }
        public string Name { get; }
        public string Genre { get; }
        public string Description { get; }
        public int Wage { get; }
        public DateTime Date { get; }
    }
}
