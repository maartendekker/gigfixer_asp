﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibary
{
    public abstract class User
    {
        public User(int id, string email, string firstName, string lastName, string infix, string telNr)
        {
            this.Id = id;
            this.Email = email;
            this.FirstName = firstName;
            this.LastName = lastName;
            this.Infix = infix;
            this.TelNr = telNr;
        }

        public int Id { get; }
        public string Email { get; }
        public string FirstName { get; }
        public string LastName { get; }
        public string Infix { get; }
        public string TelNr { get; }
        public string FullName { get; }
    }
}

